import turtle
import math

def love(t):
    x = 16 * (math.sin(t)**3)
    y = 13 * math.cos(t) - 5 * math.cos(2*t) - 2 * math.cos(3*t) - math.cos(4*t) + 2
    return x, y

turtle.setup(700, 700)
turtle.speed(0.1)
turtle.pensize(2)
turtle.bgcolor("black")
turtle.color("red")

factor1 = 20
factor2 = 20
turtle.penup()

for i in range(0, 250):
    x, y = love(i)
    turtle.goto(factor1*x, factor1*y)
    turtle.pendown()

turtle.penup()
turtle.goto(-180, 10)
turtle.pendown()
turtle.write("Happy Valentine's Day!", font=("garamond", 20, "bold"))
turtle.penup()
turtle.goto(-90, -40)
turtle.pendown()
turtle.write("From: Tomzkyy", font=("garamond", 20, "bold"))

turtle.exitonclick()